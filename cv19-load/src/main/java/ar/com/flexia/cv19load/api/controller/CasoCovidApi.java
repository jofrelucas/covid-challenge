package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.model.entity.CasoCovid;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.security.core.Authentication;

@Tag(name = "Casos Covid Load", description = "Controlador para cargar nuevos casos de covid.")
public interface CasoCovidApi {

    @Operation(summary = "Cargar un nuevo caso de covid",
            description = "Esta accion solo puede ser realizada por un cliente",
            tags = { "CasoCovid" })
    @ApiResponses(value = { @ApiResponse(
            description = "successful operation",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CasoCovid.class))
    )})
    public CasoCovid cargarCaso(@Parameter(description = "Los datos del caso a cargar.") NuevoCaso nuevoCaso,
                                Authentication auth);
}
