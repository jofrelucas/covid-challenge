package ar.com.flexia.cv19load.model.repository;

import ar.com.flexia.cv19load.model.entity.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PacienteRepository extends JpaRepository<Paciente, Long> {
    Optional<Paciente> findByNombreAndApellidoAndDni(String nombre, String apellido, Long dni);
    //empty
}
