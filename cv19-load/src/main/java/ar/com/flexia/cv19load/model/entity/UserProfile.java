package ar.com.flexia.cv19load.model.entity;

public enum UserProfile {
    ROLE_CLIENT,
    ROLE_ADMIN;
}
