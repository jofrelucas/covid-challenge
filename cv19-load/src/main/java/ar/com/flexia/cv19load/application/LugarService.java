package ar.com.flexia.cv19load.application;

import ar.com.flexia.cv19load.model.entity.Lugar;

public interface LugarService {
    Lugar crearLugar(String provincia, String municipio);
}
