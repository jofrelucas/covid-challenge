package ar.com.flexia.cv19load;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cv19LoadApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cv19LoadApplication.class, args);
    }

}
