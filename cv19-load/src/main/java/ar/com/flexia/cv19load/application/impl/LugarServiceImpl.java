package ar.com.flexia.cv19load.application.impl;

import ar.com.flexia.cv19load.application.LugarService;
import ar.com.flexia.cv19load.model.entity.Lugar;
import ar.com.flexia.cv19load.model.repository.LugarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LugarServiceImpl implements LugarService {

    private static final Logger LOG = LoggerFactory.getLogger(LugarServiceImpl.class);

    private final LugarRepository lugarRepository;

    public LugarServiceImpl(LugarRepository lugarRepository) {
        LOG.debug("Creando LugarServiceImpl Bean...");
        this.lugarRepository = lugarRepository;
    }

    @Override
    public Lugar crearLugar(String provincia, String municipio) {
        LOG.info("Creando nuevo Lugar...");
        LOG.debug("datos: provincia: {}, municipio: {}", provincia, municipio);

        return lugarRepository.findByProvinciaAndMunicipio(provincia, municipio)
        		.orElseGet(() -> lugarRepository.save(new Lugar(provincia, municipio)));
    }
}
