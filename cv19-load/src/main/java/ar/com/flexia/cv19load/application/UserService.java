package ar.com.flexia.cv19load.application;

import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.api.dto.UserCredentials;
import ar.com.flexia.cv19load.model.entity.CvUser;

import java.util.Optional;

public interface UserService {
    Optional<CvUser> findUser(String username);

    Session login(UserCredentials userCredentials);
}
