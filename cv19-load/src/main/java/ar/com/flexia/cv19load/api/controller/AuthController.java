package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.api.dto.UserCredentials;
import ar.com.flexia.cv19load.application.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth")
public class AuthController implements AuthApi {

    private UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/login")
    public Session login(@RequestBody UserCredentials userCredentials) {
        return userService.login(userCredentials);
    }
}