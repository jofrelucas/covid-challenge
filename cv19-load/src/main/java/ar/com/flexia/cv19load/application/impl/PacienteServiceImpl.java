package ar.com.flexia.cv19load.application.impl;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.application.PacienteService;
import ar.com.flexia.cv19load.model.entity.Paciente;
import ar.com.flexia.cv19load.model.repository.PacienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class PacienteServiceImpl implements PacienteService {

    private static final Logger LOG = LoggerFactory.getLogger(PacienteServiceImpl.class);

    private final PacienteRepository pacienteRepository;

    public PacienteServiceImpl(PacienteRepository pacienteRepository) {
        this.pacienteRepository = pacienteRepository;
        LOG.debug("Creando PacienteRepository Bean...");
    }

    @Override
    public Paciente crearPaciente(String nombre, String apellido, Long dni, LocalDate nacimiento, Long telefono, String email) {
        LOG.info("Creando nuevo Paciente...");
        LOG.debug("datos: nombre: {}, apellido: {}, dni: {}, nacimiento: {}, telefono: {}, email: {}", nombre, apellido, dni, nacimiento, telefono, email);
        Paciente paciente = new Paciente(nombre, apellido, dni, nacimiento, telefono, email);
        LOG.debug("Paciente id {} creado.", paciente.getId());
        return pacienteRepository.save(paciente);
    }

    @Override
    public Paciente crearPaciente(NuevoCaso nuevoCaso) {
        LOG.info("Creando nuevo Paciente...");
        LOG.debug("datos: nombre: {}, apellido: {}, dni: {}, nacimiento: {}, telefono: {}, email: {}",
                nuevoCaso.getPacienteNombre(),
                nuevoCaso.getPacienteApellido(),
                nuevoCaso.getPacienteDni(),
                nuevoCaso.getPacienteNacimiento(),
                nuevoCaso.getPacienteTelefono(),
                nuevoCaso.getPacienteEmail());


        return pacienteRepository.findByNombreAndApellidoAndDni(
                nuevoCaso.getPacienteNombre(),
                nuevoCaso.getPacienteApellido(),
                nuevoCaso.getPacienteDni())
                    .orElseGet(() -> new Paciente(nuevoCaso.getPacienteNombre(),
                        nuevoCaso.getPacienteApellido(),
                        nuevoCaso.getPacienteDni(),
                        nuevoCaso.getPacienteNacimiento(),
                        nuevoCaso.getPacienteTelefono(),
                        nuevoCaso.getPacienteEmail()));
    }
}
