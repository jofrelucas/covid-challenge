package ar.com.flexia.cv19load.api.config;

import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JWTConfig {

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_NAME = "Authorization";
    public static final String AUTHORITIES_CLAIM = "authorities";
    public static final String USERNAME_CLAIM = "username";

    private Algorithm algorithm;
    private String secret;
    private Long expiration;

    public JWTConfig(@Value("${security.jwt.secret:my-super-default-secret}") String secret,
                     @Value("${security.jwt.exp:86400}") Long expiration) {
        this.algorithm = Algorithm.HMAC512(secret);
        this.secret = secret;
        this.expiration = expiration;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public String getSecret() {
        return secret;
    }

    public Long getExpiration() {
        return expiration;
    }
}
