package ar.com.flexia.cv19load.application;

import ar.com.flexia.cv19load.model.entity.CvUser;
import com.auth0.jwt.interfaces.DecodedJWT;

public interface JWTService {

    String issueToken(CvUser cvUser);

    DecodedJWT verify(String jwt);
}
