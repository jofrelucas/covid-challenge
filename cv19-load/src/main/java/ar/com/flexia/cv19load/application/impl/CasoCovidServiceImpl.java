package ar.com.flexia.cv19load.application.impl;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.application.CasoCovidService;
import ar.com.flexia.cv19load.application.LugarService;
import ar.com.flexia.cv19load.application.PacienteService;
import ar.com.flexia.cv19load.model.entity.CasoCovid;
import ar.com.flexia.cv19load.model.entity.Lugar;
import ar.com.flexia.cv19load.model.entity.Paciente;
import ar.com.flexia.cv19load.model.repository.CasoCovidRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Service
public class CasoCovidServiceImpl implements CasoCovidService {

    private static final Logger LOG = LoggerFactory.getLogger(CasoCovidServiceImpl.class);

    private final CasoCovidRepository casoCovidRepository;
    private final LugarService lugarService;
    private final PacienteService pacienteService;

    public CasoCovidServiceImpl(CasoCovidRepository casoCovidRepository, LugarService lugarService, PacienteService pacienteService) {
        this.lugarService = lugarService;
        this.pacienteService = pacienteService;
        this.casoCovidRepository = casoCovidRepository;
        LOG.debug("Creando CasoCovidServiceImpl Bean...");
    }

    @Secured({"ROLE_CLIENT"})
    @Override
    public CasoCovid crearCaso(NuevoCaso nuevoCaso, Long idCliente) {
        LOG.info("Creando nuevo CasoCovid...");
        LOG.debug("datos: {}", nuevoCaso);
        Lugar ubicacion = lugarService.crearLugar(nuevoCaso.getUbicacionProvincia(), nuevoCaso.getUbicacionMunicipio());
        Paciente paciente = pacienteService.crearPaciente(nuevoCaso);
        CasoCovid casoCovid = new CasoCovid(idCliente, nuevoCaso.getDeteccion(), ubicacion, paciente);
        return casoCovidRepository.save(casoCovid);
    }
}
