package ar.com.flexia.cv19load.application.impl;

import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.api.dto.UserCredentials;
import ar.com.flexia.cv19load.application.JWTService;
import ar.com.flexia.cv19load.application.UserService;
import ar.com.flexia.cv19load.model.entity.CvUser;
import ar.com.flexia.cv19load.model.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, JWTService jwtService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        LOG.debug("Creando UserServiceImpl Bean");
    }

    @Override
    public Optional<CvUser> findUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Session login(UserCredentials userCredentials) {
        LOG.info("Logueando usuario...");
        LOG.debug("Credenciales: username: {}, password: {}", userCredentials.getUsername(), userCredentials.getPassword());
        CvUser cvUser = userRepository.findByUsername(userCredentials.getUsername())
                .orElseThrow(() -> new BadCredentialsException("usuario o clave invalidos"));
        if (!passwordEncoder.matches(userCredentials.getPassword(), cvUser.getPassword())) {
            throw new BadCredentialsException("usuario o clave invalidos");
        }
        String token = jwtService.issueToken(cvUser);
        LOG.debug("Login correcto, devolviendo token: {}", token);
        return new Session(token, cvUser);
    }
}
