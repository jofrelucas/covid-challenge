package ar.com.flexia.cv19load.model.repository;

import ar.com.flexia.cv19load.model.entity.CasoCovid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CasoCovidRepository extends JpaRepository<CasoCovid, Long> {
    // empty
}
