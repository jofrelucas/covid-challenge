package ar.com.flexia.cv19load.api.dto;

import java.time.LocalDate;

public class NuevoCaso {

    private LocalDate deteccion;

    private String ubicacionProvincia;

    private String ubicacionMunicipio;

    private String pacienteNombre;

    private String pacienteApellido;

    private Long pacienteDni;

    private LocalDate pacienteNacimiento;

    private Long pacienteTelefono;

    private String pacienteEmail;

    public LocalDate getDeteccion() {
        return deteccion;
    }

    public void setDeteccion(LocalDate deteccion) {
        this.deteccion = deteccion;
    }

    public void setUbicacionProvincia(String ubicacionProvincia) {
        this.ubicacionProvincia = ubicacionProvincia;
    }

    public void setUbicacionMunicipio(String ubicacionMunicipio) {
        this.ubicacionMunicipio = ubicacionMunicipio;
    }

    public void setPacienteNombre(String pacienteNombre) {
        this.pacienteNombre = pacienteNombre;
    }

    public void setPacienteApellido(String pacienteApellido) {
        this.pacienteApellido = pacienteApellido;
    }

    public void setPacienteDni(Long pacienteDni) {
        this.pacienteDni = pacienteDni;
    }

    public void setPacienteNacimiento(LocalDate pacienteNacimiento) {
        this.pacienteNacimiento = pacienteNacimiento;
    }

    public void setPacienteTelefono(Long pacienteTelefono) {
        this.pacienteTelefono = pacienteTelefono;
    }

    public void setPacienteEmail(String pacienteEmail) {
        this.pacienteEmail = pacienteEmail;
    }

    public String getUbicacionProvincia() {
        return ubicacionProvincia;
    }

    public String getUbicacionMunicipio() {
        return ubicacionMunicipio;
    }

    public String getPacienteNombre() {
        return pacienteNombre;
    }

    public String getPacienteApellido() {
        return pacienteApellido;
    }

    public Long getPacienteDni() {
        return pacienteDni;
    }

    public LocalDate getPacienteNacimiento() {
        return pacienteNacimiento;
    }

    public Long getPacienteTelefono() {
        return pacienteTelefono;
    }

    public String getPacienteEmail() {
        return pacienteEmail;
    }
}
