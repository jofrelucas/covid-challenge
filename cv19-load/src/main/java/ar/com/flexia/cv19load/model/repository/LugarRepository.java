package ar.com.flexia.cv19load.model.repository;

import ar.com.flexia.cv19load.model.entity.Lugar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LugarRepository extends JpaRepository<Lugar, Long> {
    Optional<Lugar> findByProvinciaAndMunicipio(String provincia, String municipio);

    // empty
}
