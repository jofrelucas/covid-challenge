package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.api.dto.Session;
import ar.com.flexia.cv19load.api.dto.UserCredentials;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Autorizacion", description = "Api para loguearse como cliente.")
public interface AuthApi {

    @Operation(summary = "Log in",
            description = "Esta ruta esta abierta. Devuelve una sesion que contiene el token jwt con las credenciales",
            tags = { "CvUser" })
    @ApiResponses(value = { @ApiResponse(
            description = "successful operation",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = Session.class))
    )})
    public Session login(@Parameter(description = "Las credenciales del cliente.") UserCredentials userCredentials);
}
