package ar.com.flexia.cv19load.application;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.model.entity.Paciente;

import java.time.LocalDate;

public interface PacienteService {
    Paciente crearPaciente(String nombre, String apellido, Long dni, LocalDate nacimiento, Long telefono, String email);

    Paciente crearPaciente(NuevoCaso nuevoCaso);
}
