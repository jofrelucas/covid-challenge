package ar.com.flexia.cv19load.model.repository;

import ar.com.flexia.cv19load.model.entity.CvUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<CvUser, Long> {
    Optional<CvUser> findByUsername(String username);
    // empty
}
