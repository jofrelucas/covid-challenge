package ar.com.flexia.cv19load.model.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Lugar {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_lugar")
    private Long id;

    private String provincia;

    private String municipio;

    public Lugar() {
        // for Spring Jpa
    }

    public Lugar(String provincia, String municipio) {
        this.provincia = provincia;
        this.municipio = municipio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
}
