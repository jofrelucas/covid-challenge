package ar.com.flexia.cv19load.api.controller;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.application.CasoCovidService;
import ar.com.flexia.cv19load.model.entity.CasoCovid;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/caso")
public class CasoCovidController implements CasoCovidApi {

    private final CasoCovidService casoCovidService;

    public CasoCovidController(CasoCovidService casoCovidService) {
        this.casoCovidService = casoCovidService;
    }

    @PostMapping
    public CasoCovid cargarCaso(@RequestBody NuevoCaso nuevoCaso,
                                Authentication auth) {
        Long idCliente = Long.parseLong(auth.getPrincipal().toString());
        return casoCovidService.crearCaso(nuevoCaso, idCliente);
    }
}
