package ar.com.flexia.cv19load.api.dto;

import ar.com.flexia.cv19load.model.entity.CvUser;

public class Session {

    private String token;
    private CvUser user;

    public Session(String token, CvUser user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public CvUser getUser() {
        return user;
    }
}
