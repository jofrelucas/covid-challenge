package ar.com.flexia.cv19load.api.config;

import ar.com.flexia.cv19load.application.APIException;
import ar.com.flexia.cv19load.application.ErrorTypes;
import ar.com.flexia.cv19load.application.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;


/**
 * {@link UserDetailsService} implementation that looks up a {@link User} in the database
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);

	private UserService userService;
	
	/**
	 * @param userService
	 */
	public CustomUserDetailsService(UserService userService) {
		super();
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		LOG.debug("loading user {}", username);
		return userService.findUser(username).orElseThrow(() -> new APIException(ErrorTypes.USER_NOT_FOUND));
	}
}
