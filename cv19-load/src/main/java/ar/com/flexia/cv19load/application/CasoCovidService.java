package ar.com.flexia.cv19load.application;

import ar.com.flexia.cv19load.api.dto.NuevoCaso;
import ar.com.flexia.cv19load.model.entity.CasoCovid;

public interface CasoCovidService {
    CasoCovid crearCaso(NuevoCaso nuevoCaso, Long idCliente);
}
