package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.application.CasoCovidService;
import ar.com.flexia.cv19query.model.entity.CasoCovid;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "/caso")
public class CasoCovidController implements CasoCovidApi {

    private final CasoCovidService casoCovidService;

    public CasoCovidController(CasoCovidService casoCovidService) {
        this.casoCovidService = casoCovidService;
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping
    public List<CasoCovid> obtenerUltimosCasos(@RequestParam(name = "cantidad", required = false, defaultValue = "5") Integer cantidadCasos) {
        Page<CasoCovid> page = casoCovidService.buscarUltimosCasos(cantidadCasos);
        return page.getContent();
    }

    @Secured({"ROLE_ADMIN"})
    @GetMapping(path = "/cantidad")
    public int obtenerCantidadCasos(@RequestParam(name = "desde", required = false, defaultValue = "2019-01-01") String fechaDesde,
                                    @RequestParam(name = "hasta", required = false) String fechaHasta,
                                    @RequestParam(name = "provincia", required = false) String provincia,
                                    @RequestParam(name = "municipio", required = false) String municipio) {
        LocalDate fechaDesdeParseada = LocalDate.parse(fechaDesde);
        LocalDate fechaHastaParseada;
        if (fechaHasta != null) {
            fechaHastaParseada = LocalDate.parse(fechaHasta);
        } else {
            fechaHastaParseada = LocalDate.now();
        }
        return casoCovidService.contarCasos(fechaDesdeParseada, fechaHastaParseada, provincia, municipio);
    }
}
