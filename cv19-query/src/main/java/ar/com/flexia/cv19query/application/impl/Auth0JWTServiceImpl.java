package ar.com.flexia.cv19query.application.impl;

import ar.com.flexia.cv19query.api.config.JWTConfig;
import ar.com.flexia.cv19query.application.JWTService;
import ar.com.flexia.cv19query.model.entity.CvUser;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class Auth0JWTServiceImpl implements JWTService {

    private final JWTConfig jwtConfig;
    private final JWTVerifier jwtVerifier;

    public Auth0JWTServiceImpl(JWTConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
        this.jwtVerifier = JWT.require(jwtConfig.getAlgorithm()).build();
    }

    @Override
    public String issueToken(CvUser cvUser) {
        List<String> authorities = Arrays.asList(cvUser.getProfile().name());
        String jwt = JWT.create()
                .withSubject(cvUser.getId().toString())
                .withExpiresAt(Date.from(Instant.now().plusSeconds(jwtConfig.getExpiration())))
                .withArrayClaim(JWTConfig.AUTHORITIES_CLAIM, authorities.toArray(new String[0]))
                .withClaim(JWTConfig.USERNAME_CLAIM, cvUser.getUsername())
                .sign(jwtConfig.getAlgorithm());
        return JWTConfig.TOKEN_PREFIX + jwt;
    }

    @Override
    public DecodedJWT verify(String jwt) {
        jwt = jwt.replace(JWTConfig.TOKEN_PREFIX, "").trim();
        return jwtVerifier.verify(jwt);
    }
}
