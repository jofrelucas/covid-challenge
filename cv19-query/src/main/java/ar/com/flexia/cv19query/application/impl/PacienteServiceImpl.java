package ar.com.flexia.cv19query.application.impl;

import ar.com.flexia.cv19query.application.PacienteService;
import ar.com.flexia.cv19query.model.repository.PacienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PacienteServiceImpl implements PacienteService {

    private static final Logger LOG = LoggerFactory.getLogger(PacienteServiceImpl.class);

    private final PacienteRepository pacienteRepository;

    public PacienteServiceImpl(PacienteRepository pacienteRepository) {
        this.pacienteRepository = pacienteRepository;
        LOG.debug("Creando PacienteRepository Bean...");
    }

}
