package ar.com.flexia.cv19query.application.impl;

import ar.com.flexia.cv19query.application.LugarService;
import ar.com.flexia.cv19query.model.entity.Lugar;
import ar.com.flexia.cv19query.model.repository.LugarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LugarServiceImpl implements LugarService {

    private static final Logger LOG = LoggerFactory.getLogger(LugarServiceImpl.class);

    private LugarRepository lugarRepository;

    public LugarServiceImpl(LugarRepository lugarRepository) {
        LOG.debug("Creando LugarServiceImpl Bean...");
        this.lugarRepository = lugarRepository;
    }

}
