package ar.com.flexia.cv19query.application;

import org.springframework.http.HttpStatus;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;


public enum ErrorTypes {
    INVALID_PASSWORD(HttpStatus.BAD_REQUEST, "Provided password is not valid"),

    USER_NOT_FOUND(HttpStatus.BAD_REQUEST, "User not found"),
    USER_ALREADY_EXISTS(HttpStatus.BAD_REQUEST, "User already exists"),
    ADMIN_CANNOT_BE_DELETED(HttpStatus.BAD_REQUEST, "User admin can not be deleted");


    private static final Map<HttpStatus, ErrorTypes> HTTP_STATUS = new EnumMap<>(HttpStatus.class);
    private static final Map<String, ErrorTypes> TEXT = new HashMap<>();

    static {
        for (ErrorTypes e : values()) {
            HTTP_STATUS.put(e.httpStatus, e);
            TEXT.put(e.message, e);
        }
    }

    public final HttpStatus httpStatus;
    public final String message;

    private ErrorTypes(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public static ErrorTypes valueOfCode(HttpStatus httpStatus) {
        return HTTP_STATUS.get(httpStatus);
    }

    public static ErrorTypes valueOfText(String text) {
        return TEXT.get(text);
    }
}
