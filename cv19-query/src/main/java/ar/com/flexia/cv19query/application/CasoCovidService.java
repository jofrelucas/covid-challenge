package ar.com.flexia.cv19query.application;

import ar.com.flexia.cv19query.model.entity.CasoCovid;
import org.springframework.data.domain.Page;

import java.time.LocalDate;

public interface CasoCovidService {

    int contarCasos(LocalDate fechaDesde, LocalDate fechaHasta, String provincia, String municipio);

    Page<CasoCovid> buscarUltimosCasos(Integer cantidadCasos);
}
