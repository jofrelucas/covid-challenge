package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.api.dto.NewUser;
import ar.com.flexia.cv19query.application.UserService;
import ar.com.flexia.cv19query.model.entity.CvUser;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/user")
public class UserController implements UserApi {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Secured({ "ROLE_ADMIN" })
    @PostMapping
    public CvUser createUser(@RequestBody NewUser newUser) {
        return userService.createUser(newUser);
    }
}
