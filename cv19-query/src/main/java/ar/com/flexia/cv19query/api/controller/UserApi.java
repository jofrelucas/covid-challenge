package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.api.dto.NewUser;
import ar.com.flexia.cv19query.model.entity.CvUser;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

public interface UserApi {

    @Operation(summary = "Crear nuevo Cliente",
            description = "Solo puede ser usado por el administrador",
            tags = { "CvUser" })
    @ApiResponses(value = { @ApiResponse(
            description = "successful operation",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CvUser.class))
    )})
    public CvUser createUser(@Parameter(description = "Las datos del nuevo cliente.") NewUser newUser);
}
