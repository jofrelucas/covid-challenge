package ar.com.flexia.cv19query.api.dto;

import ar.com.flexia.cv19query.model.entity.CvUser;

public class Session {

    private String token;
    private CvUser cvUser;

    public Session(String token, CvUser cvUser) {
        this.token = token;
        this.cvUser = cvUser;
    }

    public String getToken() {
        return token;
    }

    public CvUser getUser() {
        return cvUser;
    }
}
