package ar.com.flexia.cv19query.model.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class CasoCovid {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_caso")
    private Long id;

    private Long cliente;

    private LocalDate deteccion;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Lugar ubicacion;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Paciente paciente;

    public CasoCovid() {
        // for Spring Jpa
    }

    public CasoCovid(Long cliente, LocalDate deteccion, Lugar ubicacion, Paciente paciente) {
        this.cliente = cliente;
        this.deteccion = deteccion;
        this.ubicacion = ubicacion;
        this.paciente = paciente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCliente() {
        return cliente;
    }

    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }

    public LocalDate getDeteccion() {
        return deteccion;
    }

    public void setDeteccion(LocalDate deteccion) {
        this.deteccion = deteccion;
    }

    public Lugar getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Lugar ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
