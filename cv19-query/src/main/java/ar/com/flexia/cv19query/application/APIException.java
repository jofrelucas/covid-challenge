package ar.com.flexia.cv19query.application;

import org.springframework.http.HttpStatus;

public class APIException extends RuntimeException{
    private final HttpStatus httpStatus;

    public APIException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public APIException(HttpStatus httpStatus, String message, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public APIException(ErrorTypes error) {
        super(error.message);
        this.httpStatus = error.httpStatus;
    }

    public APIException(ErrorTypes error, Throwable cause) {
        super(error.message, cause);
        this.httpStatus = error.httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
