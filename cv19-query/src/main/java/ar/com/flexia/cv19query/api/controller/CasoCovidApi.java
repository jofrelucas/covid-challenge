package ar.com.flexia.cv19query.api.controller;

import ar.com.flexia.cv19query.model.entity.CasoCovid;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@Tag(name = "CasosCovidQuery", description = "Endopoint para realizar consultas sobre los casos de covid.")
public interface CasoCovidApi {

    @Operation(summary = "Ultimos N casos registrados",
            description = "Solo puede ser usado por el admin.", tags = { "CasoCovid" })
    @ApiResponses(value = { @ApiResponse(
            description = "successful operation",
            content = @Content(mediaType = "application/json", schema = @Schema(implementation = CasoCovid.class))
    )})
    public List<CasoCovid> obtenerUltimosCasos(@Parameter(description = "Cantidad de casos solicitados.") Integer cantidadCasos);


    @Operation(summary = "Cantidad de casos segun parametros de busqueda. En caso de no haber parametros se devuelven los casos desde el 2019-01-01 de todo el pais.",
            description = "Solo puede ser usado por el admin.", tags = { "CasoCovid" })
    @ApiResponses(value = { @ApiResponse(
            description = "successful operation",
            content = @Content(mediaType = "text")
    )})
    public int obtenerCantidadCasos(@Parameter(description = "fecha desde cuando filtrar.") String fechaDesde,
                                    @Parameter(description = "fecha hasta cuando filtrar.") String fechaHasta,
                                    @Parameter(description = "provincia para filtrar.") String provincia,
                                    @Parameter(description = "municipio para filtrar.") String municipio);

}
