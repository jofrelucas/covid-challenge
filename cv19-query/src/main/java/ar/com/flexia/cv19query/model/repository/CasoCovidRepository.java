package ar.com.flexia.cv19query.model.repository;

import ar.com.flexia.cv19query.model.entity.CasoCovid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CasoCovidRepository extends JpaRepository<CasoCovid, Long> {

    @Query(value = "select count(*) from caso_covid where deteccion between ? and ?", nativeQuery = true)
    int contarEntreFechas(LocalDate fechaDesde, LocalDate fechaHasta);

    // Version SQL
    @Query(value = "select count(*) from caso_covid as c join lugar l on c.ubicacion_id = l.id where c.deteccion between ? and ? and l.provincia like ? and l.municipio like ?", nativeQuery = true)
    int contarEntreFechasConProvinciaYLocalidad1(LocalDate fechaDesde, LocalDate fechaHasta, String provincia, String localidad);

    // Version JPQL
    @Query("select count(c) from CasoCovid c where c.deteccion between ?1 and ?2 and c.ubicacion.provincia like ?3 and c.ubicacion.municipio like ?4")
    int contarEntreFechasConProvinciaYLocalidad(LocalDate fechaDesde, LocalDate fechaHasta, String provincia, String localidad);

    @Query("select c from CasoCovid c order by c.deteccion desc")
    Page<CasoCovid> buscarTodosOrdenadosPorFecha(Pageable pageable);
}
