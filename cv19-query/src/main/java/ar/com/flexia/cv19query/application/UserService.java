package ar.com.flexia.cv19query.application;

import ar.com.flexia.cv19query.api.dto.NewUser;
import ar.com.flexia.cv19query.api.dto.Session;
import ar.com.flexia.cv19query.api.dto.UserCredentials;
import ar.com.flexia.cv19query.model.entity.CvUser;

import java.util.Optional;

public interface UserService {
    CvUser createUser(NewUser newUser);

    Optional<CvUser> findUser(String username);

    Session login(UserCredentials userCredentials);
}
