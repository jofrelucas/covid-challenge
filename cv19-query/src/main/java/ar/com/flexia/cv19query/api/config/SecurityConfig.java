package ar.com.flexia.cv19query.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.List;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private List<String> corsAllowedOrigins;

    private CustomUserDetailsService userDetailsService;

    private PasswordEncoder passwordEncoder;

    /**
     * @param corsAllowedOrigins
     * @param userDetailsService
     * @param passwordEncoder
     */
    public SecurityConfig(@Value("#{'${cors.allowed-origins:*}'.split(',')}") List<String> corsAllowedOrigins,
                          CustomUserDetailsService userDetailsService,
                          PasswordEncoder passwordEncoder) {
        super();
        this.corsAllowedOrigins = corsAllowedOrigins;
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors()
                .and().csrf().disable()
                .headers().frameOptions().sameOrigin()
                .and().authorizeRequests()
                .antMatchers("/h2", "/h2/*").permitAll()
                .antMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                .antMatchers("/auth/login").permitAll()
                .anyRequest().authenticated()
                .and().addFilter(jwtAuthorizationFilter())
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Bean
    public JWTAuthorizationFilter jwtAuthorizationFilter() throws Exception {
        return new JWTAuthorizationFilter(this.authenticationManager());
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        List<String> allowedMethods = new ArrayList<>();
        allowedMethods.add(CorsConfiguration.ALL);
        CorsConfiguration corsConfig = new CorsConfiguration().applyPermitDefaultValues();
        corsConfig.setAllowedMethods(allowedMethods);
        corsConfig.setAllowedOrigins(corsAllowedOrigins);
        corsConfig.setAllowCredentials(true);
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig);
        return source;
    }
}
