package ar.com.flexia.cv19query.application.impl;

import ar.com.flexia.cv19query.application.CasoCovidService;
import ar.com.flexia.cv19query.application.LugarService;
import ar.com.flexia.cv19query.application.PacienteService;
import ar.com.flexia.cv19query.model.entity.CasoCovid;
import ar.com.flexia.cv19query.model.repository.CasoCovidRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CasoCovidServiceImpl implements CasoCovidService {

    private static final Logger LOG = LoggerFactory.getLogger(CasoCovidServiceImpl.class);

    private final CasoCovidRepository casoCovidRepository;
    private final LugarService lugarService;
    private final PacienteService pacienteService;

    public CasoCovidServiceImpl(CasoCovidRepository casoCovidRepository, LugarService lugarService, PacienteService pacienteService) {
        this.lugarService = lugarService;
        this.pacienteService = pacienteService;
        this.casoCovidRepository = casoCovidRepository;
        LOG.debug("Creando CasoCovidServiceImpl Bean...");
    }

    @Override
    public int contarCasos(LocalDate fechaDesde, LocalDate fechaHasta, String provincia, String municipio) {
        int cantidad;
        if (provincia == null && municipio == null) {
            cantidad = casoCovidRepository.contarEntreFechas(fechaDesde, fechaHasta);
        } else {
            provincia = "%" + provincia + "%";
            municipio = "%" + municipio + "%";
            cantidad = casoCovidRepository.contarEntreFechasConProvinciaYLocalidad(fechaDesde, fechaHasta, provincia, municipio);
        }
        return cantidad;
    }

    @Override
    public Page<CasoCovid> buscarUltimosCasos(Integer cantidadCasos) {
        PageRequest pageRequest = PageRequest.of(0, cantidadCasos);
        return casoCovidRepository.buscarTodosOrdenadosPorFecha(pageRequest);
    }
}
