package ar.com.flexia.cv19query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cv19QueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cv19QueryApplication.class, args);
    }

}
