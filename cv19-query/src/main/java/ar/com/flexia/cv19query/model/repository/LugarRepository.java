package ar.com.flexia.cv19query.model.repository;

import ar.com.flexia.cv19query.model.entity.Lugar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LugarRepository extends JpaRepository<Lugar, Long> {

    // empty
}
