package ar.com.flexia.cv19query.application.impl;

import ar.com.flexia.cv19query.api.dto.NewUser;
import ar.com.flexia.cv19query.api.dto.Session;
import ar.com.flexia.cv19query.api.dto.UserCredentials;
import ar.com.flexia.cv19query.application.JWTService;
import ar.com.flexia.cv19query.application.UserService;
import ar.com.flexia.cv19query.model.entity.CvUser;
import ar.com.flexia.cv19query.model.entity.UserProfile;
import ar.com.flexia.cv19query.model.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JWTService jwtService;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, JWTService jwtService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        LOG.debug("Creando UserServiceImpl Bean");
        if (this.userRepository.count() == 0) {
            LOG.debug("Creando Admin.");
            CvUser admin = new CvUser("admin", this.passwordEncoder.encode("admin"), UserProfile.ROLE_ADMIN);
            this.userRepository.save(admin);
        }
    }

    @Override
    public CvUser createUser(NewUser newUser) {
        LOG.info("Creando nuevo usuario...");
        LOG.debug("Usuario info: username: {}, password: {}", newUser.getUsername(), newUser.getPassword());

        Optional<CvUser> userExists = userRepository.findByUsername(newUser.getUsername());
        if (userExists.isPresent()) {
            LOG.debug("Nombre de usuario {} ya existe", newUser.getUsername());
            // todo
//            throw new APIException(ErrorTypes.USUARIO_YA_EXISTE);
            throw new RuntimeException("usuario repetido");
        }

        String encodedPassword = passwordEncoder.encode(newUser.getPassword());
        CvUser cvUser = new CvUser(newUser.getUsername(), encodedPassword, UserProfile.ROLE_CLIENT);
        return userRepository.save(cvUser);
    }

    @Override
    public Optional<CvUser> findUser(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Session login(UserCredentials userCredentials) {
        LOG.info("Logueando usuario...");
        LOG.debug("Credenciales: username: {}, password: {}", userCredentials.getUsername(), userCredentials.getPassword());
        CvUser cvUser = userRepository.findByUsername(userCredentials.getUsername())
                .orElseThrow(() -> new BadCredentialsException("usuario o clave invalidos"));
        if (!passwordEncoder.matches(userCredentials.getPassword(), cvUser.getPassword())) {
            throw new BadCredentialsException("usuario o clave invalidos");
        }
        String token = jwtService.issueToken(cvUser);
        LOG.debug("Login correcto, devolviendo token: {}", token);
        return new Session(token, cvUser);
    }
}
