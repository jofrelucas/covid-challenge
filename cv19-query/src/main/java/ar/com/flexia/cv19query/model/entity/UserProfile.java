package ar.com.flexia.cv19query.model.entity;

public enum UserProfile {
    ROLE_CLIENT,
    ROLE_ADMIN;
}
